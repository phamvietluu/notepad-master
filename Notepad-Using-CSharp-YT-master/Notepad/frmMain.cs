﻿using NotepadCore;
using System.Windows.Forms;
using System;
using Microsoft.VisualBasic;
using Notepad.Forms;
using System.Diagnostics;

namespace Notepad
{
    public partial class frmMain : Form
    {
        FileOperation fileOperation;
        EditOperation editOperation;
        Timer timer;
        frmFind formFind;
        frmReplace formReplace;
        string filter = "Text Documents (*.txt)|*.txt|All Files (*.*)|*.*";
        int percent = 100;
        public EditOperation EditOperation
        {
            get
            {
                return editOperation;
            }

            set
            {
                editOperation = value;
            }
        }

        public frmMain()
        {
            InitializeComponent();
            fileOperation = new FileOperation();
            editOperation = new EditOperation();
            formFind = new frmFind(this);
            formFind.Editor = txtArea;
            fileOperation.InitializeNewFile();
            this.Text = fileOperation.Filename;
            timer = new Timer();
            timer.Tick += Mytimer_Tick;
            timer.Interval = 500;
            txtArea.HideSelection = false;
        }

        private void Mytimer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            editOperation.Add_UndoRedo(txtArea.Text);
            UpdateView();
        }
        private void newFileMenu_Click(object sender, System.EventArgs e)
        {
            if (fileOperation.IsFileSaved)
            {
                txtArea.Text = "";
                fileOperation.InitializeNewFile();
                UpdateView();
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you need save changes to " + fileOperation.Filename, "Notepad", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (fileOperation.Filename.Contains("Untitled"))
                    {
                        SaveAsFile();
                    }
                    else
                    {
                        fileOperation.SaveFile(fileOperation.FileLocation, txtArea.Lines);
                        UpdateView();
                    }
                    fileOperation.InitializeNewFile();
                    txtArea.Text = "";
                }
                else if (result == DialogResult.No)
                {
                    txtArea.Text = "";
                    fileOperation.InitializeNewFile();
                    UpdateView();
                }
            }
        }

        private void UpdateView()
        {
            this.Text = !fileOperation.IsFileSaved ? "*" + fileOperation.Filename: fileOperation.Filename;
        }

        private void txtArea_TextChanged(object sender, EventArgs e)
        {
            fileOperation.IsFileSaved = false;
            if (editOperation.TxtAreaTextChangeRequired)
            {
                timer.Start();
            }
            else
            {
                editOperation.TxtAreaTextChangeRequired = false;
            }
            findEditMenu.Enabled = txtArea.Text.Length > 0 ? true : false;
            findnextEditMenu.Enabled = txtArea.Text.Length > 0 ? true : false;
            findpreEditMenu.Enabled = txtArea.Text.Length > 0 ? true : false;
            UpdateView();
        }

        private void openFileMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = this.filter;
            openFile.Title = "Open";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                txtArea.TextChanged -= txtArea_TextChanged;
                txtArea.Text = fileOperation.OpenFile(openFile.FileName);
                txtArea.TextChanged += txtArea_TextChanged;
                UpdateView();
            }
        }

        private void saveFileMenu_Click(object sender, EventArgs e)
        {
            if(fileOperation.FileLocation == null)
            {
                SaveAsFile();
            }
            else
            {
                fileOperation.SaveFile(fileOperation.FileLocation, txtArea.Lines);
                UpdateView();
            }
        }

        //lưu file và hiển thị file đã lưu
        private void SaveAsFile()
        {
            SaveFileDialog fileSave = new SaveFileDialog();
            fileSave.Title = "Save As";
            fileSave.FileName = "*.txt";
            fileSave.Filter = this.filter;
            if (fileSave.ShowDialog() == DialogResult.OK)
            {
                fileOperation.SaveFile(fileSave.FileName, txtArea.Lines);
                UpdateView();
            }
        }

        private void saveasFileMenu_Click(object sender, EventArgs e)
        {
            SaveAsFile();
        }

        private void exitFileMenu_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void editMenu_Click(object sender, EventArgs e)
        {
            cutEditMenu.Enabled = txtArea.SelectedText.Length > 0 ? true : false;
            copyEditMenu.Enabled = txtArea.SelectedText.Length > 0 ? true : false;
            deleteEditMenu.Enabled = txtArea.SelectedText.Length > 0 ? true : false;
            pasteEditMenu.Enabled = Clipboard.GetDataObject().GetDataPresent(DataFormats.Text);

            undoEditMenu.Enabled = editOperation.CanUndo() ? true : false;

        }

        private void editMenu_MouseEnter(object sender, EventArgs e)
        {
            editMenu_Click(sender, e);
        }

        private void cutEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.Cut();
        }

        private void copyEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.Copy();
        }

        private void pasteEditMenu_Click(object sender, EventArgs e)
        {
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text))
                txtArea.Paste();
        }

        private void selectallEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.SelectAll();
        }

        private void deleteEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.Text = txtArea.Text.Remove(txtArea.SelectionStart, txtArea.SelectionLength);
        }

        private void timedateEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.Text = txtArea.Text.Insert(txtArea.SelectionStart, editOperation.DateTime_Now());
        }

        private void undoEditMenu_Click(object sender, EventArgs e)
        {
            txtArea.Text = editOperation.UndoClicked();
            UpdateView();
        }

        private void gotoEditMenu_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Line Number", "Go To", "1");
            try
            {
                int line = Convert.ToInt32(input);
                if (line > txtArea.Lines.Length)
                {
                    MessageBox.Show("Total lines in the file is " + txtArea.Lines.Length, "Can't Reach",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    string[] lines = txtArea.Lines;
                    int len = 0;
                    for (int i = 0; i < line - 1; i++)
                    {
                        len = len + lines[i].Length + 1;
                    }
                    txtArea.Focus();
                    txtArea.Select(len, 0);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Enter a valid Integer", "Wrong Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void findEditMenu_Click(object sender, EventArgs e)
        {
            if (formFind == null)
            {
                formFind = new frmFind(this);
                formFind.Editor = txtArea;
            }
            formFind.Show();
        }

        private void findnextEditMenu_Click(object sender, EventArgs e)
        {
            formFind.UpdateSearchQuery_DOWN();
            if (formFind.Query.SearchString.Length == 0)
            {
                formFind.Show();
            }
            else
            {
                FindNextResult result = editOperation.FindNext(formFind.Query);
                if (result.SearchStatus)
                    txtArea.Select(result.SelectionStart, formFind.Query.SearchString.Length);
                else
                {
                    MessageBox.Show("Cannot find " + "\"" + formFind.Query.SearchString + "\"", "Notepad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        private void wordwrapFormatMenu_CheckedChanged(object sender, EventArgs e)
        {
            txtArea.WordWrap = wordwrapFormatMenu.Checked;
            if(txtArea.WordWrap)
                txtArea.ScrollBars = RichTextBoxScrollBars.ForcedHorizontal;
        }

        private void statusbarViewMenu_CheckedChanged(object sender, EventArgs e)
        {
            statusContent.Visible = statusbarViewMenu.Checked;
        }

        private void txtArea_SelectionChanged(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            int pos = txtArea.SelectionStart;
            int line = txtArea.GetLineFromCharIndex(pos) + 1;
            int col = pos - txtArea.GetFirstCharIndexOfCurrentLine() + 1;

            status.Text = "Ln " + line + ", Col " + col;
        }

        private void replaceEditMenu_Click(object sender, EventArgs e)
        {
            if (formReplace == null)
            {
                formReplace = new frmReplace();
                formReplace.Editor = txtArea;
                formReplace.editOpertion = editOperation;
            }
            formReplace.Show();
        }

        private void fontFormatMenu_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog = new FontDialog();
            fontDialog.ShowColor = true;
            fontDialog.Font = txtArea.Font;
            fontDialog.Color = txtArea.ForeColor;
            if (fontDialog.ShowDialog() != DialogResult.Cancel)
            {
                txtArea.Font = fontDialog.Font;
                txtArea.ForeColor = fontDialog.Color;
            }
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.bing.com/search?q=get+help+with+notepad+in+windows+10&filters=guid:%224466414-en-dia%22%20lang:%22en%22&form=T00032&ocid=HelpPane-BingIA");
        }

        private void aboutnotepadHelpMenu_Click(object sender, EventArgs e)
        {
            frmAbout aboutForm = new frmAbout();
            aboutForm.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmMain main = new frmMain();
            main.Show();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.bing.com/search?q=" + txtArea.Text.Trim());
        }

        private void findpreEditMenu_Click(object sender, EventArgs e)
        {
            formFind.UpdateSearchQuery_UP();
            if (formFind.Query.SearchString.Length == 0)
            {
                formFind.Show();
            }
            else
            {
                FindNextResult result = editOperation.FindNext(formFind.Query);
                if (result.SearchStatus)
                    txtArea.Select(result.SelectionStart, formFind.Query.SearchString.Length);
                else
                {
                    MessageBox.Show("Cannot find " + "\"" + formFind.Query.SearchString + "\"", "Notepad", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void wordwrapFormatMenu_Click(object sender, EventArgs e)
        {

        }

        private void statusbarViewMenu_Click(object sender, EventArgs e)
        {

        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (percent <= 10)
                return;
            percent -= 10;
            this.zoom.Text = percent.ToString() + "%";
            if (percent > 100)
                txtArea.ZoomFactor = txtArea.ZoomFactor - 0.2f;
            else
                txtArea.ZoomFactor = txtArea.ZoomFactor - 0.1f;
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (percent <= 100)
                txtArea.ZoomFactor = txtArea.ZoomFactor + ((100 - percent) / 10) * 0.1f;
            else
                txtArea.ZoomFactor = txtArea.ZoomFactor - ((percent - 100) / 10) * 0.2f;
            this.zoom.Text = "100%";
            this.percent = 100;
        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (percent >= 500)
                return;
            percent += 10;
            this.zoom.Text = percent.ToString() + "%";
            if (percent > 100)
                txtArea.ZoomFactor = txtArea.ZoomFactor + 0.2f;
            else
                txtArea.ZoomFactor = txtArea.ZoomFactor + 0.1f;
        }
    }
}
