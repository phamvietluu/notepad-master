﻿namespace Notepad
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menubar = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveasFileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitFileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.undoEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.copyEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.findEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.findnextEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.findpreEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.selectallEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.timedateEditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.formatMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.wordwrapFormatMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fontFormatMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusbarViewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewhelpHelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutnotepadHelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusContent = new System.Windows.Forms.StatusStrip();
            this.status = new System.Windows.Forms.ToolStripStatusLabel();
            this.zoom = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtArea = new System.Windows.Forms.RichTextBox();
            this.menubar.SuspendLayout();
            this.statusContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // menubar
            // 
            this.menubar.BackColor = System.Drawing.Color.White;
            this.menubar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.formatMenu,
            this.viewMenu,
            this.viewhelpHelpMenu});
            this.menubar.Location = new System.Drawing.Point(0, 0);
            this.menubar.Name = "menubar";
            this.menubar.Padding = new System.Windows.Forms.Padding(0);
            this.menubar.Size = new System.Drawing.Size(748, 24);
            this.menubar.TabIndex = 0;
            this.menubar.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileMenu,
            this.toolStripMenuItem1,
            this.openFileMenu,
            this.saveFileMenu,
            this.saveasFileMenu,
            this.toolStripSeparator1,
            this.exitFileMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 24);
            this.fileMenu.Text = "File";
            // 
            // newFileMenu
            // 
            this.newFileMenu.BackColor = System.Drawing.SystemColors.Control;
            this.newFileMenu.Name = "newFileMenu";
            this.newFileMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newFileMenu.Size = new System.Drawing.Size(220, 22);
            this.newFileMenu.Text = "New";
            this.newFileMenu.Click += new System.EventHandler(this.newFileMenu_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(220, 22);
            this.toolStripMenuItem1.Text = "New Window";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click_1);
            // 
            // openFileMenu
            // 
            this.openFileMenu.Name = "openFileMenu";
            this.openFileMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openFileMenu.Size = new System.Drawing.Size(220, 22);
            this.openFileMenu.Text = "Open...";
            this.openFileMenu.Click += new System.EventHandler(this.openFileMenu_Click);
            // 
            // saveFileMenu
            // 
            this.saveFileMenu.Name = "saveFileMenu";
            this.saveFileMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveFileMenu.Size = new System.Drawing.Size(220, 22);
            this.saveFileMenu.Text = "Save";
            this.saveFileMenu.Click += new System.EventHandler(this.saveFileMenu_Click);
            // 
            // saveasFileMenu
            // 
            this.saveasFileMenu.Name = "saveasFileMenu";
            this.saveasFileMenu.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveasFileMenu.Size = new System.Drawing.Size(220, 22);
            this.saveasFileMenu.Text = "Save As...";
            this.saveasFileMenu.Click += new System.EventHandler(this.saveasFileMenu_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(217, 6);
            // 
            // exitFileMenu
            // 
            this.exitFileMenu.Name = "exitFileMenu";
            this.exitFileMenu.Size = new System.Drawing.Size(220, 22);
            this.exitFileMenu.Text = "Exit";
            this.exitFileMenu.Click += new System.EventHandler(this.exitFileMenu_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoEditMenu,
            this.toolStripSeparator3,
            this.cutEditMenu,
            this.copyEditMenu,
            this.pasteEditMenu,
            this.deleteEditMenu,
            this.toolStripSeparator4,
            this.toolStripMenuItem2,
            this.findEditMenu,
            this.findnextEditMenu,
            this.findpreEditMenu,
            this.replaceEditMenu,
            this.gotoEditMenu,
            this.toolStripSeparator5,
            this.selectallEditMenu,
            this.timedateEditMenu});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 24);
            this.editMenu.Text = "Edit";
            this.editMenu.Click += new System.EventHandler(this.editMenu_Click);
            this.editMenu.MouseEnter += new System.EventHandler(this.editMenu_MouseEnter);
            // 
            // undoEditMenu
            // 
            this.undoEditMenu.Enabled = false;
            this.undoEditMenu.Name = "undoEditMenu";
            this.undoEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoEditMenu.Size = new System.Drawing.Size(211, 22);
            this.undoEditMenu.Text = "Undo";
            this.undoEditMenu.Click += new System.EventHandler(this.undoEditMenu_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(208, 6);
            // 
            // cutEditMenu
            // 
            this.cutEditMenu.Enabled = false;
            this.cutEditMenu.Name = "cutEditMenu";
            this.cutEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutEditMenu.Size = new System.Drawing.Size(211, 22);
            this.cutEditMenu.Text = "Cut";
            this.cutEditMenu.Click += new System.EventHandler(this.cutEditMenu_Click);
            // 
            // copyEditMenu
            // 
            this.copyEditMenu.Enabled = false;
            this.copyEditMenu.Name = "copyEditMenu";
            this.copyEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyEditMenu.Size = new System.Drawing.Size(211, 22);
            this.copyEditMenu.Text = "Copy";
            this.copyEditMenu.Click += new System.EventHandler(this.copyEditMenu_Click);
            // 
            // pasteEditMenu
            // 
            this.pasteEditMenu.Enabled = false;
            this.pasteEditMenu.Name = "pasteEditMenu";
            this.pasteEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteEditMenu.Size = new System.Drawing.Size(211, 22);
            this.pasteEditMenu.Text = "Paste";
            this.pasteEditMenu.Click += new System.EventHandler(this.pasteEditMenu_Click);
            // 
            // deleteEditMenu
            // 
            this.deleteEditMenu.Enabled = false;
            this.deleteEditMenu.Name = "deleteEditMenu";
            this.deleteEditMenu.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteEditMenu.Size = new System.Drawing.Size(211, 22);
            this.deleteEditMenu.Text = "Delete";
            this.deleteEditMenu.Click += new System.EventHandler(this.deleteEditMenu_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(208, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.toolStripMenuItem2.Size = new System.Drawing.Size(211, 22);
            this.toolStripMenuItem2.Text = "Search with Bing...";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // findEditMenu
            // 
            this.findEditMenu.Enabled = false;
            this.findEditMenu.Name = "findEditMenu";
            this.findEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findEditMenu.Size = new System.Drawing.Size(211, 22);
            this.findEditMenu.Text = "Find...";
            this.findEditMenu.Click += new System.EventHandler(this.findEditMenu_Click);
            // 
            // findnextEditMenu
            // 
            this.findnextEditMenu.Enabled = false;
            this.findnextEditMenu.Name = "findnextEditMenu";
            this.findnextEditMenu.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.findnextEditMenu.Size = new System.Drawing.Size(211, 22);
            this.findnextEditMenu.Text = "Find Next";
            this.findnextEditMenu.Click += new System.EventHandler(this.findnextEditMenu_Click);
            // 
            // findpreEditMenu
            // 
            this.findpreEditMenu.Enabled = false;
            this.findpreEditMenu.Name = "findpreEditMenu";
            this.findpreEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.findpreEditMenu.Size = new System.Drawing.Size(211, 22);
            this.findpreEditMenu.Text = "Find Previous";
            this.findpreEditMenu.Click += new System.EventHandler(this.findpreEditMenu_Click);
            // 
            // replaceEditMenu
            // 
            this.replaceEditMenu.Name = "replaceEditMenu";
            this.replaceEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.replaceEditMenu.Size = new System.Drawing.Size(211, 22);
            this.replaceEditMenu.Text = "Replace...";
            this.replaceEditMenu.Click += new System.EventHandler(this.replaceEditMenu_Click);
            // 
            // gotoEditMenu
            // 
            this.gotoEditMenu.Name = "gotoEditMenu";
            this.gotoEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.gotoEditMenu.Size = new System.Drawing.Size(211, 22);
            this.gotoEditMenu.Text = "Go To...";
            this.gotoEditMenu.Click += new System.EventHandler(this.gotoEditMenu_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(208, 6);
            // 
            // selectallEditMenu
            // 
            this.selectallEditMenu.Name = "selectallEditMenu";
            this.selectallEditMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectallEditMenu.Size = new System.Drawing.Size(211, 22);
            this.selectallEditMenu.Text = "Select All";
            this.selectallEditMenu.Click += new System.EventHandler(this.selectallEditMenu_Click);
            // 
            // timedateEditMenu
            // 
            this.timedateEditMenu.Name = "timedateEditMenu";
            this.timedateEditMenu.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.timedateEditMenu.Size = new System.Drawing.Size(211, 22);
            this.timedateEditMenu.Text = "Time/Date";
            this.timedateEditMenu.Click += new System.EventHandler(this.timedateEditMenu_Click);
            // 
            // formatMenu
            // 
            this.formatMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wordwrapFormatMenu,
            this.fontFormatMenu});
            this.formatMenu.Name = "formatMenu";
            this.formatMenu.Size = new System.Drawing.Size(57, 24);
            this.formatMenu.Text = "Format";
            // 
            // wordwrapFormatMenu
            // 
            this.wordwrapFormatMenu.Checked = true;
            this.wordwrapFormatMenu.CheckOnClick = true;
            this.wordwrapFormatMenu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.wordwrapFormatMenu.Name = "wordwrapFormatMenu";
            this.wordwrapFormatMenu.Size = new System.Drawing.Size(134, 22);
            this.wordwrapFormatMenu.Text = "Word Wrap";
            this.wordwrapFormatMenu.CheckedChanged += new System.EventHandler(this.wordwrapFormatMenu_CheckedChanged);
            this.wordwrapFormatMenu.Click += new System.EventHandler(this.wordwrapFormatMenu_Click);
            // 
            // fontFormatMenu
            // 
            this.fontFormatMenu.Name = "fontFormatMenu";
            this.fontFormatMenu.Size = new System.Drawing.Size(134, 22);
            this.fontFormatMenu.Text = "Font...";
            this.fontFormatMenu.Click += new System.EventHandler(this.fontFormatMenu_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.statusbarViewMenu});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 24);
            this.viewMenu.Text = "View";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInToolStripMenuItem,
            this.zoomOutToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(126, 22);
            this.toolStripMenuItem3.Text = "Zoom";
            // 
            // zoomInToolStripMenuItem
            // 
            this.zoomInToolStripMenuItem.Name = "zoomInToolStripMenuItem";
            this.zoomInToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Oemplus)));
            this.zoomInToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.zoomInToolStripMenuItem.Text = "Zoom In";
            this.zoomInToolStripMenuItem.Click += new System.EventHandler(this.zoomInToolStripMenuItem_Click);
            // 
            // zoomOutToolStripMenuItem
            // 
            this.zoomOutToolStripMenuItem.Name = "zoomOutToolStripMenuItem";
            this.zoomOutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.OemMinus)));
            this.zoomOutToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.zoomOutToolStripMenuItem.Text = "Zoom Out";
            this.zoomOutToolStripMenuItem.Click += new System.EventHandler(this.zoomOutToolStripMenuItem_Click);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D0)));
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.restoreToolStripMenuItem.Text = "Restore Default Zoom";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.restoreToolStripMenuItem_Click);
            // 
            // statusbarViewMenu
            // 
            this.statusbarViewMenu.Checked = true;
            this.statusbarViewMenu.CheckOnClick = true;
            this.statusbarViewMenu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusbarViewMenu.Name = "statusbarViewMenu";
            this.statusbarViewMenu.Size = new System.Drawing.Size(126, 22);
            this.statusbarViewMenu.Text = "Status Bar";
            this.statusbarViewMenu.CheckedChanged += new System.EventHandler(this.statusbarViewMenu_CheckedChanged);
            this.statusbarViewMenu.Click += new System.EventHandler(this.statusbarViewMenu_Click);
            // 
            // viewhelpHelpMenu
            // 
            this.viewhelpHelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewHelpToolStripMenuItem,
            this.aboutnotepadHelpMenu});
            this.viewhelpHelpMenu.Name = "viewhelpHelpMenu";
            this.viewhelpHelpMenu.Size = new System.Drawing.Size(44, 24);
            this.viewhelpHelpMenu.Text = "Help";
            // 
            // viewHelpToolStripMenuItem
            // 
            this.viewHelpToolStripMenuItem.Name = "viewHelpToolStripMenuItem";
            this.viewHelpToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewHelpToolStripMenuItem.Text = "View Help";
            this.viewHelpToolStripMenuItem.Click += new System.EventHandler(this.viewHelpToolStripMenuItem_Click);
            // 
            // aboutnotepadHelpMenu
            // 
            this.aboutnotepadHelpMenu.Name = "aboutnotepadHelpMenu";
            this.aboutnotepadHelpMenu.Size = new System.Drawing.Size(180, 22);
            this.aboutnotepadHelpMenu.Text = "About Notepad";
            this.aboutnotepadHelpMenu.Click += new System.EventHandler(this.aboutnotepadHelpMenu_Click);
            // 
            // statusContent
            // 
            this.statusContent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status,
            this.zoom});
            this.statusContent.Location = new System.Drawing.Point(0, 421);
            this.statusContent.Name = "statusContent";
            this.statusContent.Size = new System.Drawing.Size(748, 22);
            this.statusContent.TabIndex = 2;
            this.statusContent.Text = "statusStrip1";
            // 
            // status
            // 
            this.status.Margin = new System.Windows.Forms.Padding(0, 3, 50, 2);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(62, 17);
            this.status.Text = "Ln 1, Col 1";
            // 
            // zoom
            // 
            this.zoom.Name = "zoom";
            this.zoom.Size = new System.Drawing.Size(35, 17);
            this.zoom.Text = "100%";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(748, 2);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(5, 395);
            this.panel2.TabIndex = 4;
            // 
            // txtArea
            // 
            this.txtArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArea.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArea.Location = new System.Drawing.Point(5, 26);
            this.txtArea.Name = "txtArea";
            this.txtArea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.txtArea.Size = new System.Drawing.Size(743, 395);
            this.txtArea.TabIndex = 5;
            this.txtArea.Text = "";
            this.txtArea.SelectionChanged += new System.EventHandler(this.txtArea_SelectionChanged);
            this.txtArea.TextChanged += new System.EventHandler(this.txtArea_TextChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 443);
            this.Controls.Add(this.txtArea);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusContent);
            this.Controls.Add(this.menubar);
            this.MainMenuStrip = this.menubar;
            this.Name = "MainForm";
            this.Text = "Notepad";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menubar.ResumeLayout(false);
            this.menubar.PerformLayout();
            this.statusContent.ResumeLayout(false);
            this.statusContent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menubar;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newFileMenu;
        private System.Windows.Forms.ToolStripMenuItem openFileMenu;
        private System.Windows.Forms.ToolStripMenuItem saveFileMenu;
        private System.Windows.Forms.ToolStripMenuItem saveasFileMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitFileMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem formatMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewhelpHelpMenu;
        private System.Windows.Forms.ToolStripMenuItem undoEditMenu;
        private System.Windows.Forms.ToolStripMenuItem cutEditMenu;
        private System.Windows.Forms.ToolStripMenuItem copyEditMenu;
        private System.Windows.Forms.ToolStripMenuItem pasteEditMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteEditMenu;
        private System.Windows.Forms.ToolStripMenuItem findEditMenu;
        private System.Windows.Forms.ToolStripMenuItem findnextEditMenu;
        private System.Windows.Forms.ToolStripMenuItem replaceEditMenu;
        private System.Windows.Forms.ToolStripMenuItem gotoEditMenu;
        private System.Windows.Forms.ToolStripMenuItem selectallEditMenu;
        private System.Windows.Forms.ToolStripMenuItem timedateEditMenu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem wordwrapFormatMenu;
        private System.Windows.Forms.ToolStripMenuItem fontFormatMenu;
        private System.Windows.Forms.ToolStripMenuItem statusbarViewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutnotepadHelpMenu;
        private System.Windows.Forms.StatusStrip statusContent;
        private System.Windows.Forms.ToolStripStatusLabel status;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox txtArea;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem findpreEditMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel zoom;
    }
}

