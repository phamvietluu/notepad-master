﻿using NotepadCore;
using System;
using System.Windows.Forms;

namespace Notepad.Forms
{
    public partial class frmReplace : Form
    {
        FindNextSearch query = new FindNextSearch();
        public RichTextBox Editor;
        public EditOperation editOpertion;

        public FindNextSearch Query
        {
            get
            {
                return query;
            }

            set
            {
                query = value;
            }
        }

        public frmReplace()
        {
            InitializeComponent();
        }

        private void FormReplace_Load(object sender, EventArgs e)
        {
            DisableButtons();
        }



        private void DisableButtons()
        {
            if (txtFind.Text.Length == 0)
            {
                btnFind.Enabled = btnReplace.Enabled = btnReplaceAll.Enabled = false;
            }
            else
            {
                btnFind.Enabled = btnReplace.Enabled = btnReplaceAll.Enabled = true;
            }
        }

        private void txtFind_TextChanged(object sender, EventArgs e)
        {
            DisableButtons();
        }
        public void UpdateSearchQuery()
        {
            query.SearchString = txtFind.Text;
            query.Direction = "DOWN";
            query.MatchCase = chkMatchCase.Checked;
            query.WrapAround = chkWrapAround.Checked;
            query.Content = Editor.Text;
            query.Position = Editor.SelectionStart;
        }
        private void btnFind_Click(object sender, EventArgs e)
        {
            UpdateSearchQuery();
            FindNextResult result = editOpertion.FindNext(this.query);
            if (result.SearchStatus)
                this.Editor.Select(result.SelectionStart, txtFind.Text.Length);
            else
            {
                MessageBox.Show("Cannot find " + "\"" + txtFind.Text + "\"", "Notepad", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            if (Editor.SelectionLength == 0)
                btnFind.PerformClick();
            else
                Editor.SelectedText = txtReplace.Text;
        }

        private void btnReplaceAll_Click(object sender, EventArgs e)
        {
            Editor.Text = Editor.Text.Replace(txtFind.Text, txtReplace.Text);
        }
    }
}
