﻿using NotepadCore;
using System;

using System.Windows.Forms;

namespace Notepad.Forms
{
    public partial class frmFind : Form
    {
        frmMain mainForm;
        EditOperation editOpertion;
        FindNextSearch query = new FindNextSearch();

        public RichTextBox Editor { get; internal set; }
        public FindNextSearch Query { get => query; set => query = value; }

        public frmFind(frmMain mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
            oDown.Checked = true;
            editOpertion = mainForm.EditOperation;
            Query.Success = false;
            btnFindNext.Enabled = false;
        }

        private void txtFind_TextChanged(object sender, EventArgs e)
        {
            btnFindNext.Enabled = (txtFind.Text.Length > 0) ? true : false;
            UpdateSearchQuery();
        }

        public void UpdateSearchQuery()
        {
            Query.SearchString = txtFind.Text;
            Query.Direction = oUp.Checked ? "UP" : "DOWN";
            Query.MatchCase = chkMatchCase.Checked;
            Query.WrapAround = chkWarpAround.Checked;
            Query.Content = Editor.Text;
            Query.Position = Editor.SelectionStart;
        }

        public void UpdateSearchQuery_UP()
        {
            Query.SearchString = txtFind.Text;
            Query.Direction = "UP";
            Query.MatchCase = chkMatchCase.Checked;
            Query.WrapAround = chkWarpAround.Checked;
            Query.Content = Editor.Text;
            Query.Position = Editor.SelectionStart;
        }

        public void UpdateSearchQuery_DOWN()
        {
            Query.SearchString = txtFind.Text;
            Query.Direction = "DOWN";
            Query.MatchCase = chkMatchCase.Checked;
            Query.WrapAround = chkWarpAround.Checked;
            Query.Content = Editor.Text;
            Query.Position = Editor.SelectionStart;
        }

        private void chkMatchCase_CheckedChanged(object sender, EventArgs e)
        {
            UpdateSearchQuery();
        }

        private void oUp_CheckedChanged(object sender, EventArgs e)
        {
            UpdateSearchQuery();
        }

        private void FormFind_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        private void btnFindNext_Click(object sender, EventArgs e)
        {
            UpdateSearchQuery();
            FindNextResult result = editOpertion.FindNext(Query);
            if (result.SearchStatus)
                Editor.Select(result.SelectionStart, txtFind.Text.Length);
            else
            {
                MessageBox.Show("Cannot find " + "\"" + txtFind.Text + "\"", "Notepad", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void FormFind_Load(object sender, EventArgs e)
        {

        }
    }
}
