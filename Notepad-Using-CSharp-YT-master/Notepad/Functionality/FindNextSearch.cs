﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotepadCore
{
    public class FindNextSearch
    {
        string searchString;
        string direction;
        bool matchCase;
        string content;
        int position;
        bool success;
        bool wrapAround;

        public string SearchString { get => searchString; set => searchString = value; }
        public string Direction { get => direction; set => direction = value; }
        public bool MatchCase { get => matchCase; set => matchCase = value; }
        public string Content { get => content; set => content = value; }
        public int Position { get => position; set => position = value; }
        public bool Success { get => success; set => success = value; }
        public bool WrapAround { get => wrapAround; set => wrapAround = value; }
    }
}
